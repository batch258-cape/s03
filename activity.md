1. What is the name of the package used to install the MongoDB Server?
```
apt-get
```

2. What is the command used to determine the status of the MongoDB Server?
```
sudo systemctl status mongod
```

3. What is the default port used by MongoDB Server?
```
27017
```

4. What is the setting name in configuration file of MongoDB Serveresponsible for allowing access other than localhost (127.0.0.1)?
```
bindIp
```

5. What is the name of the command used to connect to a MongoDB Server while using a terminal?
```
mongosh mongodb://localhost:27017
```

6. Specify the complete location of the MongoDB Server configuration file.
```
etc/mongod.conf 
```

7. Changes made in the configuration file of MongoDB Server are automatically applied.
```
no
```

8. What is the command used to copy a file?
```
cp
```

9. What is the file used to declare an application's environment variables?
```
.env
```

10. What is the first command to be used before starting a Node.js application?
```
npm install
```

11. What is the protocol used to enable inbound connection to port 27017 and 4000?
```
TCP
```

12. The PM2 package is what kind of program according to its home page?
```
daemon process manager
```